# Not initial anymore

My result of the experiment:

# Summary of Experiment

## Task A
| Experiment       	| Accuracy      | F1    | Precision | Recall|
| ------------- |:-------------:| -----:|----------:|------:|
| Random Forest (Baseline) (1 gram)| 0.6361 | 0.6212 | 0.6666 | 0.5816| 
| XGBoost (1 gram)     | 0.6387     | 0.6567   | 0.6407|0.6734 |
| GRU + fastext| 0.6512      |     |||
| GRU + sentic | 0.5755 | | | |
| GRU + sentic + fastext  | 0.6948 ||||

## Task B
| Experiment       	| Accuracy      | F1    | Precision | Recall|
| ------------- |:-------------:| -----:|----------:|------:|
| Random Forest (Baseline) (1 gram)| 0.5994 | 0.5422 | 0.5140 | 0.5994| 
| XGBoost (1 gram)     | 0.5811     | 0.5236   |  0.6051|0.5811 |